/*
Navicat MySQL Data Transfer

Source Server         : localhostt3306
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : data_source0

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2020-04-28 10:56:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for code_relate0
-- ----------------------------
DROP TABLE IF EXISTS `code_relate0`;
CREATE TABLE `code_relate0` (
  `relate_id` bigint(20) unsigned NOT NULL COMMENT '关联id',
  `stack_code` varchar(255) NOT NULL COMMENT '垛码,跟task_upload的stack_code关联',
  `box_code` varchar(255) NOT NULL COMMENT '箱码',
  `bottle_code` varchar(255) NOT NULL COMMENT '瓶码',
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id,跟task的主键关联',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`relate_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='瓶箱垛关联表（绑定表子表）';

-- ----------------------------
-- Records of code_relate0
-- ----------------------------

-- ----------------------------
-- Table structure for code_relate1
-- ----------------------------
DROP TABLE IF EXISTS `code_relate1`;
CREATE TABLE `code_relate1` (
  `relate_id` bigint(20) unsigned NOT NULL COMMENT '关联id',
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id,跟task的主键关联',
  `stack_code` varchar(255) NOT NULL COMMENT '垛码,跟task_upload的stack_code关联',
  `box_code` varchar(255) NOT NULL COMMENT '箱码',
  `bottle_code` varchar(255) NOT NULL COMMENT '瓶码',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`relate_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='瓶箱垛关联表（绑定表子表）';

-- ----------------------------
-- Records of code_relate1
-- ----------------------------

-- ----------------------------
-- Table structure for customer0
-- ----------------------------
DROP TABLE IF EXISTS `customer0`;
CREATE TABLE `customer0` (
  `customer_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `customer_name` varchar(255) NOT NULL COMMENT '用户姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表（水平拆分表）';

-- ----------------------------
-- Records of customer0
-- ----------------------------
INSERT INTO `customer0` VALUES ('1254704101002960897', '天天', '2020-04-27 17:29:04', '2020-04-27 17:29:04');

-- ----------------------------
-- Table structure for customer1
-- ----------------------------
DROP TABLE IF EXISTS `customer1`;
CREATE TABLE `customer1` (
  `customer_id` bigint(20) unsigned NOT NULL COMMENT '用户id',
  `customer_name` varchar(255) NOT NULL COMMENT '用户姓名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表（水平拆分表）';

-- ----------------------------
-- Records of customer1
-- ----------------------------
INSERT INTO `customer1` VALUES ('1254704101023932418', '小李', '2020-04-27 17:29:04', '2020-04-27 17:29:04');

-- ----------------------------
-- Table structure for factory
-- ----------------------------
DROP TABLE IF EXISTS `factory`;
CREATE TABLE `factory` (
  `factory_id` bigint(20) unsigned NOT NULL COMMENT '工厂id',
  `factory_name` varchar(255) NOT NULL COMMENT '工厂名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`factory_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工厂表（广播表）';

-- ----------------------------
-- Records of factory
-- ----------------------------
INSERT INTO `factory` VALUES ('1254693839277441026', '富土康', '2020-04-27 16:49:06', '2020-04-27 16:49:06');
INSERT INTO `factory` VALUES ('1254693839717842945', '血汗工厂', '2020-04-27 16:49:07', '2020-04-27 16:49:07');
INSERT INTO `factory` VALUES ('1254693839755591681', '深圳厂', '2020-04-27 16:49:07', '2020-04-27 16:49:07');

-- ----------------------------
-- Table structure for indent0
-- ----------------------------
DROP TABLE IF EXISTS `indent0`;
CREATE TABLE `indent0` (
  `indent_id` bigint(20) unsigned NOT NULL COMMENT '订单id',
  `customer_id` bigint(20) unsigned NOT NULL COMMENT '用户id,跟customer的主键关联',
  `warehouse_id` bigint(20) unsigned NOT NULL COMMENT '仓库id,跟warehouse主键关联',
  `indent_name` varchar(255) NOT NULL COMMENT '订单名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`indent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表（绑定表父表）';

-- ----------------------------
-- Records of indent0
-- ----------------------------

-- ----------------------------
-- Table structure for indent1
-- ----------------------------
DROP TABLE IF EXISTS `indent1`;
CREATE TABLE `indent1` (
  `indent_id` bigint(20) unsigned NOT NULL COMMENT '订单id',
  `customer_id` bigint(20) unsigned NOT NULL COMMENT '用户id,跟customer的主键关联',
  `warehouse_id` bigint(20) unsigned NOT NULL COMMENT '仓库id,跟warehouse主键关联',
  `indent_name` varchar(255) NOT NULL COMMENT '订单名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`indent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表（绑定表父表）';

-- ----------------------------
-- Records of indent1
-- ----------------------------

-- ----------------------------
-- Table structure for indent_detail0
-- ----------------------------
DROP TABLE IF EXISTS `indent_detail0`;
CREATE TABLE `indent_detail0` (
  `detail_id` bigint(20) unsigned NOT NULL COMMENT '详情id',
  `indent_id` bigint(20) unsigned NOT NULL COMMENT '订单id',
  `customer_id` bigint(20) unsigned NOT NULL COMMENT '用户id,跟customer的主键关联',
  `product_name` varchar(255) NOT NULL COMMENT '商品名称',
  `num` int(10) unsigned DEFAULT '0' COMMENT '数量',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`detail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单详情表（绑定表子表）';

-- ----------------------------
-- Records of indent_detail0
-- ----------------------------

-- ----------------------------
-- Table structure for indent_detail1
-- ----------------------------
DROP TABLE IF EXISTS `indent_detail1`;
CREATE TABLE `indent_detail1` (
  `detail_id` bigint(20) unsigned NOT NULL COMMENT '详情id',
  `indent_id` bigint(20) unsigned NOT NULL COMMENT '订单id',
  `customer_id` bigint(20) unsigned NOT NULL COMMENT '用户id,跟customer的主键关联',
  `product_name` varchar(255) NOT NULL COMMENT '商品名称',
  `num` int(10) unsigned DEFAULT '0' COMMENT '数量',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`detail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单详情表（绑定表子表）';

-- ----------------------------
-- Records of indent_detail1
-- ----------------------------

-- ----------------------------
-- Table structure for task0
-- ----------------------------
DROP TABLE IF EXISTS `task0`;
CREATE TABLE `task0` (
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id',
  `task_name` varchar(255) NOT NULL COMMENT '任务名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务表（水平拆分表）';

-- ----------------------------
-- Records of task0
-- ----------------------------
INSERT INTO `task0` VALUES ('1254963753871532034', '任务2', '2020-04-28 10:40:49', '2020-04-28 10:40:49');
INSERT INTO `task0` VALUES ('1254963753909280770', '任务4', '2020-04-28 10:40:49', '2020-04-28 10:40:49');

-- ----------------------------
-- Table structure for task1
-- ----------------------------
DROP TABLE IF EXISTS `task1`;
CREATE TABLE `task1` (
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id',
  `task_name` varchar(255) NOT NULL COMMENT '任务名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务表（水平拆分表）';

-- ----------------------------
-- Records of task1
-- ----------------------------
INSERT INTO `task1` VALUES ('1254963753246580737', '任务1', '2020-04-28 10:40:49', '2020-04-28 10:40:49');

-- ----------------------------
-- Table structure for task_upload0
-- ----------------------------
DROP TABLE IF EXISTS `task_upload0`;
CREATE TABLE `task_upload0` (
  `upload_id` bigint(20) unsigned NOT NULL COMMENT '上传id',
  `factory_id` bigint(20) unsigned NOT NULL COMMENT '工厂id,跟factory的主键关联',
  `stack_code` varchar(255) NOT NULL COMMENT '跺码(UUID)',
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id,跟task的主键关联',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`upload_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务上传表（绑定表子表）';

-- ----------------------------
-- Records of task_upload0
-- ----------------------------

-- ----------------------------
-- Table structure for task_upload1
-- ----------------------------
DROP TABLE IF EXISTS `task_upload1`;
CREATE TABLE `task_upload1` (
  `upload_id` bigint(20) unsigned NOT NULL COMMENT '上传id',
  `factory_id` bigint(20) unsigned NOT NULL COMMENT '工厂id,跟factory的主键关联',
  `stack_code` varchar(255) NOT NULL COMMENT '跺码(UUID)',
  `task_id` bigint(20) unsigned NOT NULL COMMENT '任务id,跟task的主键关联',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`upload_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务上传表（绑定表子表）';

-- ----------------------------
-- Records of task_upload1
-- ----------------------------

-- ----------------------------
-- Table structure for warehouse
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `warehouse_id` bigint(20) unsigned NOT NULL COMMENT '仓库id',
  `warehouse_name` varchar(255) NOT NULL COMMENT '仓库名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`warehouse_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库表（广播表）';

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('1254696127794561025', '湖南仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254696127807143937', '北京仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254696127945555970', '深圳仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254696127962333186', '广西仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254697904958873602', '湖南仓1', '2020-04-27 17:04:26', '2020-04-27 17:04:26');
INSERT INTO `warehouse` VALUES ('1254697905369915394', '北京仓1', '2020-04-27 17:04:26', '2020-04-27 17:04:26');
INSERT INTO `warehouse` VALUES ('1254697905382498306', '深圳仓1', '2020-04-27 17:04:26', '2020-04-27 17:04:26');
