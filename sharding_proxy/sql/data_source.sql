/*
Navicat MySQL Data Transfer

Source Server         : localhostt3306
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : data_source

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2020-04-28 10:56:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'id',
  `area_code` varchar(255) NOT NULL COMMENT '地区编号',
  `area_name` varchar(255) NOT NULL COMMENT '地区名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区表（单库单表）';

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1254701196241920001', '110000', '北京市', '2020-04-27 17:17:31', '2020-04-27 17:17:31');
INSERT INTO `area` VALUES ('1254701196669739010', '110101', '东城区', '2020-04-27 17:17:31', '2020-04-27 17:17:31');
INSERT INTO `area` VALUES ('1254701196686516225', '110102', '西城区', '2020-04-27 17:17:31', '2020-04-27 17:17:31');
INSERT INTO `area` VALUES ('1254701196694904834', '110106', '丰台区', '2020-04-27 17:17:31', '2020-04-27 17:17:31');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `code` varchar(255) NOT NULL COMMENT '编号',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表（单库单表,UUID主键）';

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('0239346ce56848bcad8fbe35f538177e', '停止', '2020-04-27 17:20:16', '2020-04-27 17:20:16');
INSERT INTO `config` VALUES ('53e56acb513c4cd7afabf87f4eea29f3', '结束', '2020-04-27 17:20:16', '2020-04-27 17:20:16');
INSERT INTO `config` VALUES ('60afc71f7fca4c38b4a6cc9e137b31b0', '结束', '2020-04-27 17:20:45', '2020-04-27 17:20:45');
INSERT INTO `config` VALUES ('647074dcc5f34f2a96984d1e12a6ddb7', '关闭', '2020-04-27 17:20:16', '2020-04-27 17:20:16');
INSERT INTO `config` VALUES ('780e670d38084046a000094e86ca66e9', '停止', '2020-04-27 17:20:45', '2020-04-27 17:20:45');
INSERT INTO `config` VALUES ('812c9437858c406d8438ba3ea1d009c8', '静止', '2020-04-27 17:20:16', '2020-04-27 17:20:16');
INSERT INTO `config` VALUES ('9e6a63ac693446aa9a93995c9642a8be', '开始', '2020-04-27 17:20:16', '2020-04-27 17:20:16');
INSERT INTO `config` VALUES ('d35fb3c26d24474486c4a27937cae8ba', '关闭', '2020-04-27 17:20:45', '2020-04-27 17:20:45');
INSERT INTO `config` VALUES ('e31db4dcb29d4d688150992ddb0b7036', '静止', '2020-04-27 17:20:45', '2020-04-27 17:20:45');
INSERT INTO `config` VALUES ('f6aa244c57454ffabcdc67b8e3ca52f8', '开始', '2020-04-27 17:20:45', '2020-04-27 17:20:45');

-- ----------------------------
-- Table structure for factory
-- ----------------------------
DROP TABLE IF EXISTS `factory`;
CREATE TABLE `factory` (
  `factory_id` bigint(20) unsigned NOT NULL COMMENT '工厂id',
  `factory_name` varchar(255) NOT NULL COMMENT '工厂名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`factory_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工厂表（广播表）';

-- ----------------------------
-- Records of factory
-- ----------------------------
INSERT INTO `factory` VALUES ('1254693839277441026', '富土康', '2020-04-27 16:49:06', '2020-04-27 16:49:06');
INSERT INTO `factory` VALUES ('1254693839717842945', '血汗工厂', '2020-04-27 16:49:07', '2020-04-27 16:49:07');
INSERT INTO `factory` VALUES ('1254693839755591681', '深圳厂', '2020-04-27 16:49:07', '2020-04-27 16:49:07');

-- ----------------------------
-- Table structure for warehouse
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `warehouse_id` bigint(20) unsigned NOT NULL COMMENT '仓库id',
  `warehouse_name` varchar(255) NOT NULL COMMENT '仓库名称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`warehouse_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库表（广播表）';

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('1254696127794561025', '湖南仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254696127807143937', '北京仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254696127945555970', '深圳仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254696127962333186', '广西仓', '2020-04-27 16:57:22', '2020-04-27 16:57:22');
INSERT INTO `warehouse` VALUES ('1254697904958873602', '湖南仓1', '2020-04-27 17:04:26', '2020-04-27 17:04:26');
INSERT INTO `warehouse` VALUES ('1254697905369915394', '北京仓1', '2020-04-27 17:04:26', '2020-04-27 17:04:26');
INSERT INTO `warehouse` VALUES ('1254697905382498306', '深圳仓1', '2020-04-27 17:04:26', '2020-04-27 17:04:26');
