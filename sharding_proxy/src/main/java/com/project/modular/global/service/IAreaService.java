package com.project.modular.global.service;

import com.project.modular.global.entity.Area;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 地区表（单库单表） 服务类
 * </p>
 *
 * @author huangjg
 * @since 2020-04-27
 */
public interface IAreaService extends IService<Area> {

}
